<?php

 include ("db.php");

    dbIni();
  
    //Query para buscar cliente
    $sql = "SELECT *, CONCAT(EXTRACT(MONTH FROM now()),'/',EXTRACT(DAY FROM now()),'/',EXTRACT(YEAR FROM now()),' ',EXTRACT(HOURS FROM now()),':',EXTRACT(MINUTES FROM now()),':',EXTRACT(SECONDS FROM now())::integer) AS ahora FROM public.proyectos WHERE proyecto_id='0';";
    $query = pg_query($sql) or die('La consulta fallo: ' . pg_last_error());
    
    $row = pg_fetch_array($query, null, PGSQL_ASSOC);
    $tickets = $row["transas"];
    $balance = $row["balance"];
    $consultado= $row["ahora"];
    $fecha= $row["fecha"];
    
    pg_free_result($query);
      
    //Termina la conexión a la base de datos.
    dbFin();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ATW</title>
        <link rel="stylesheet" href="/media/css/materialize.min.css">
        <link rel="stylesheet" href="/media/css/main.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
 	<script>var countDownDate = Date.UTC(<?php print substr($fecha, 0, 4).",".
                            (intval(substr($fecha, 5, 2))-1).",".
                            substr($fecha, 8, 2).",".substr($fecha, 11, 2).",".
                            substr($fecha, 14, 2).",".  substr($fecha, 17, 2); ?>);</script>
        <script src="/media/fs.js"></script>
    </head>
    <body>
	<nav>
    <div class="nav-wrapper container">
      <a href="#!" class="brand-logo">Logo</a>
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
	<li><a href="/index.php">Index</a></li>
        <li><a href="/infr.php">Income</a></li>
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="mobile-demo">
	<li><a href="/index.php">Index</a></li>
        <li><a href="/infr.php">Income</a></li>
  </ul>
       <div class="container">
            <article>
                <h1>Anon Token Web Transactions</h1>
                <div id="clockdiv">
                    <!-- <?php print substr($fecha, 0, 4).",".
                            (intval(substr($fecha, 5, 2))-1).",".
                            substr($fecha, 8, 2).",".substr($fecha, 11, 2).",".
                            substr($fecha, 14, 2).",".  substr($fecha, 17, 2); ?>  -->
                    <p>Time to finish income:</p>
                    <div>
                        <span id="days">?</span>
                        <div class="smalltext">Days</div>
                    </div>
                    <div>
                        <span id="hours">?</span>
                        <div class="smalltext">Hours</div>
                    </div>
                    <div>
                        <span id="minutes">?</span>
                        <div class="smalltext">Minutes</div>
                    </div>
                    <div>
                        <span id="seconds">?</span>
                        <div class="smalltext">Seconds</div>
                    </div>
                </div>
            </article>
        </div>
        <script>
	 document.addEventListener('DOMContentLoaded', function() {
    	   var elems = document.querySelectorAll('.sidenav');
    	   var instances = M.Sidenav.init(elems);
	  });
	</script>
	<script src="/media/js/materialize.min.js"></script>
    </body>
</html>
