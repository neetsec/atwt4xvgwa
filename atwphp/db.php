<?php
/**
 * Dar formato y Presentar Reporte Diario.
 * @namespace PHP/Database
 * @package /Portal/Database
 * @access public
 * @author Alhega: Alvaro Hernández García.
 * @version 1.0
 * @since 1.0
 * @uses seguridad()
 * @uses chkPermiso()
 */

/**
 * Almacena la conexión a la base de datos.
 * @var resource $con
 */
$con=null;

/**
 * Inicia la conexión a la base de datos.
 * @uses $con Guarda la conexión a la base de datos.
 * @access public
 * @author Ahgala: Alvaro Hernández.
 * @version 1.0
 * @since 1.0
 * @source 24 5 db.php
 * @example \home\ahgala\mim\clienteRD\login.php 41 2 Llamada de la función.
 */
function dbIni() {
	global $con;
	   // Conectando y seleccionado la base de datos  
   $con = pg_connect("host=10.142.53.11 port=5432 dbname=trans user=trans password=trans")
            or die('No se ha podido conectar: ' . pg_last_error());
}

/**
 * Cierra la conexión a la base de datos.
 * @uses $con Guarda la conexión a la base de datos.
 * @access public
 * @author Ahgala: Alvaro Hernández.
 * @version 1.0
 * @since 1.0
 * @source 39 4 db.php
 * @example \home\ahgala\mim\clienteRD\login.php 98 2 Llamada de la función.
 */
function dbFin() {
	global $con;
	   pg_close($con);
}
?>
