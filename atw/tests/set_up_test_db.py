import sys
from datetime import datetime

import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT


sql_create_table = ('''
       DROP TABLE IF EXISTS txrep;

       CREATE TABLE txrep(
        urlhash CHAR(16) NOT NULL,
        wallet CHAR(34) NOT NULL,
        address_rec CHAR(34) NOT NULL,
        momento TIMESTAMP NOT NULL DEFAULT NOW(),
        status INTEGER NOT NULL DEFAULT 1,
        CONSTRAINT txrep_pk PRIMARY KEY (urlhash)
       );
       ''')

urlhash = '85a04b0b0aa85faa'
wallet = 'DCP9BYmr8F2gKYXpUMTMSQh9YsEEF4auir'
address_rec = 'DCP9BYmr8F2gKYXpUMTMSQh9YsEEF4pfka'
momento = datetime.now()
status = 1

sql_insert_row = ('''
        INSERT INTO txrep(urlhash,wallet,address_rec,momento,status)
        VALUES('{0}','{1}','{2}','{3}',{4})
        ON CONFLICT (urlhash) DO NOTHING;
        '''.format(urlhash, wallet, address_rec, momento, status))


class TestDB:
    def create_db(self):
        conn = psycopg2.connect("dbname='postgres' host='localhost' \
                                 user='molo' password=''")
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        try:
            cur.execute('CREATE DATABASE testdb;')
            cur.execute('GRANT ALL PRIVILEGES ON DATABASE testdb TO test;')
            conn.commit()
        except:
            print('Database exists. Continuing..')
        finally:
            cur.close()
            conn.close()

    def connect_db(self):
        try:
            self.conn = psycopg2.connect("dbname='testdb' host='localhost' \
                                            user='test' password=''")
            self.cur = self.conn.cursor()
        except:
            sys.exit('Unable to connect to database.')

    def disconnect_db(self):
        self.cur.close()
        self.conn.close()

    def execute_sql(self, sql):
        self.cur.execute(sql)
        self.conn.commit()


if __name__ == '__main__':
    session = TestDB()
    session.create_db()
    session.connect_db()
    session.execute_sql(sql_create_table)
    session.execute_sql(sql_insert_row)
    session.disconnect_db()
