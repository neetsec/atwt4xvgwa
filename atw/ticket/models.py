from django.db import models


class TicketRep(models.Model):
    
    urlhash = models.CharField(max_length=16, primary_key=True)

    wallet = models.CharField(max_length=34)

    address_rec = models.CharField(max_length=34)

    momento = models.DateTimeField()

    status = models.IntegerField()
    
    proyecto_id = models.IntegerField()

    def __str__(self):
        return '{}'.format(self.urlhash)

    class Meta:
        db_table = 'txrep'

        ordering = ['-momento']

        get_latest_by = 'momento'

class Proyecto(models.Model):

    proyecto_id = models.IntegerField(primary_key=True)

    account = models.CharField(max_length=250)

    balance = models.FloatField()

    fecha = models.DateTimeField()

    ticket_price = models.FloatField()

    def __str__(self):
        return '{}'.format(self.account)

    class Meta:
        db_table = 'proyectos'

        ordering = ['proyecto_id']

        get_latest_by = 'proyecto_id'
